import pkg_resources
import random
import datetime

def get_file(name):
    return pkg_resources.resource_string(__name__, 'random_values/' + name).decode('utf-8')


first_names = get_file('first_names.txt').splitlines()
def get_first_name():
    return random.choice(first_names)

last_names = get_file('last_names.txt').splitlines()
def get_last_name():
    return random.choice(last_names)

cities = get_file('cities.txt').splitlines()
def get_city():
    return random.choice(cities)

serial_number_chars = '1234567890CFGHJKLMNPRTVWXY'
serial_number_first_chars = 'LMNPRTVWXY'
def get_serial_number():
    return random.choice(serial_number_first_chars) \
        + ''.join([random.choice(serial_number_chars) for i in range(4)])

def get_authority_id():
    return ''.join([random.choice(serial_number_chars) for i in range(4)])

def get_birth_date():
    current_year = datetime.datetime.now().year
    year = random.randint(current_year-80, current_year-19)
    month = random.randint(1,12)
    #TODO: better date generation
    day = random.randint(1,28)
    return (year, month, day)

def get_exp_date():
    current_year = datetime.datetime.now().year
    year = random.randint(current_year+2, current_year+5)
    month = random.randint(1,12)
    day = random.randint(1,28)
    return (year, month, day)

eye_colors = get_file('eye_colors.txt').splitlines()
def get_eye_color():
    return random.choice(eye_colors)

def get_height():
    return random.randint(160, 190)

authorities = get_file('authorities.txt').splitlines()
def get_authority():
    authority_id, _, authority_name = random.choice(authorities).partition(' ')
    authority_name = authority_name.replace('\\n', '\n')
    return (authority_id, authority_name)