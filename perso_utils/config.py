import argparse
from perso_utils import print as persoprint

args = None

parser = argparse.ArgumentParser()
parser.set_defaults(func=parser.print_help)

subparsers = parser.add_subparsers(title='commands')
parser_print = subparsers.add_parser('print', help='Print ID card(s)')
parser_print.set_defaults(func=persoprint.main)
parser_print.add_argument('-n', '--number', type=int, help='Number of ID cards to generate', default=1)
parser_print.add_argument('-f', '--first_name', type=str, help='First name', default=None)
parser_print.add_argument('-l', '--last_name', type=str, help='Last name', default=None)

args = parser.parse_args()