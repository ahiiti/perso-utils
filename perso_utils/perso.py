import itertools
import random
import datetime
import perso_utils.random_values as gen


class Perso:
    first_name = None
    last_name = None
    birth = None
    birth_place = None
    expiry = None
    eye_color = None
    height = None
    authority = None
    authority_id = None
    serial_number = None

    def __init__(self, first_name:str=None, last_name:str=None,
                birth:(int,int,int)=None, birth_place:str=None,
                expiry:(int,int,int)=None,
                eye_color:str=None, height:int=None,
                authority:str=None, authority_id:str=None,
                serial_number:str=None
                ):
        self.first_name = first_name if first_name\
            else gen.get_first_name()

        self.last_name = last_name if last_name\
            else gen.get_last_name()

        self.birth = birth if birth\
            else gen.get_birth_date()
        
        self.birth_place = birth_place if birth_place\
            else gen.get_city()
        
        self.expiry = expiry if expiry\
            else gen.get_exp_date()
        
        self.eye_color = eye_color if eye_color\
            else gen.get_eye_color()
        
        self.height = height if height\
            else gen.get_height()
        
        if not authority_id and not authority:
            self.authority_id, self.authority = gen.get_authority()
        else:
            self.authority_id = authority_id if authority_id\
                else gen.get_authority()[0]
            self.authority = authority if authority\
                else gen.get_authority()[1]
        
        self.serial_number = serial_number if serial_number\
            else gen.get_serial_number()

    def id_number(self):
        return f'{self.authority_id}{self.serial_number}'

    mr_replacements = {
        'ä': 'ae',
        'ö': 'oe',
        'ü': 'ue',
        'ß': 'ss'
    }

    def mr_convert(self, text: str) -> str:
        for key in self.mr_replacements:
            text = text.replace(key, self.mr_replacements[key])
            text = text.replace(key.upper(), self.mr_replacements[key])
        return text.upper()

    checksum_weights = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    def checksum(self, text: str) -> str:
        checksum_quantifiers = {self.checksum_weights[i]:i for i in range(len(self.checksum_weights))}
        text = text.replace('<', '')
        sum = 0
        for (value, weight) in zip(text, itertools.cycle([7,3,1])):
            sum += checksum_quantifiers[value] * weight
        return str(sum % 10)


    def mr_area(self) -> (str, str, str):
        line1 = f'IDD<<{self.id_number()}{self.checksum(self.id_number())}'.ljust(30, '<')
        birth = f'{self.birth[0]%100:02}{self.birth[1]:02}{self.birth[2]:02}'
        exp = f'{self.expiry[0]%100:02}{self.expiry[1]:02}{self.expiry[2]:02}'
        line2_start = f'{birth}{self.checksum(birth)}<{exp}{self.checksum(exp)}'
        line2 = (line2_start + 'D').ljust(29, '<') + self.checksum(self.id_number()+self.checksum(self.id_number())+line2_start)
        line3 = f'{self.mr_convert(self.last_name)}<<{self.mr_convert(self.first_name)}'.ljust(30, '<')

        return line1, line2, line3

    def ascii_art(self) -> str:
        birth = f'{self.birth[2]:02}.{self.birth[1]:02}.{self.birth[0]:04}'
        expiry = f'{self.expiry[2]:02}.{self.expiry[1]:02}.{self.expiry[0]:04}'
        mr_area = self.mr_area()
        authority_lines = self.authority.splitlines()
        authority_line1 = authority_lines[0]
        authority_line2 = '' if len(authority_lines) < 2 else authority_lines[1]
        lines = []
        lines.append( '/----------------------------------------------------\\')
        lines.append( '| BANANENREPUBLIK DEUTSCHLAND                        |')
        lines.append(f'| PERSONALAUSWEIS       {self.id_number().rjust(28)} |')
        lines.append( '|                Name                                |')
        lines.append(f'|  XXXXXXXXXXXX  {self.last_name.upper().ljust(36)  }|')   
        lines.append(f'|  XXXXXXXXXXXX  Vorname                             |')
        lines.append(f'|  XXXXXXXXXXXX  {self.first_name.upper().ljust(36) }|')
        lines.append( '|  XXXXXXXXXXXX  Geburtstag                          |')
        lines.append(f'|  XXXXXXXXXXXX  {birth.ljust(36)                   }|')
        lines.append( '|  XXXXXXXXXXXX  Geburtsort                          |')
        lines.append(f'|  XXXXXXXXXXXX  {self.birth_place.upper().ljust(36)}|')
        lines.append( '|  XXXXXXXXXXXX  Gültig bis                          |')
        lines.append(f'|                {expiry.ljust(36)                  }|')
        lines.append('\\----------------------------------------------------/')
        lines.append('')
        lines.append( '/----------------------------------------------------\\')
        lines.append( '| Augenfarbe                                         |')
        lines.append(f'| {self.eye_color.upper().ljust(50)                } |')
        lines.append(f'| Größe                                              |')
        lines.append(f'| {(str(self.height) + " cm").ljust(50)            } |')
        lines.append( '| Behörde                                            |')
        lines.append(f'| {authority_line1.ljust(50)                       } |')
        lines.append(f'| {authority_line2.ljust(50)                       } |')
        lines.append(f'|                                                    |')
        lines.append(f'|                                                    |')
        lines.append(f'|           {mr_area[0]                  }           |')
        lines.append(f'|           {mr_area[1]                  }           |')
        lines.append(f'|           {mr_area[2]                  }           |')
        lines.append('\\----------------------------------------------------/')
        return '\n'.join(lines)
