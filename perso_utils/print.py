from perso_utils.perso import Perso
from perso_utils import config

def main():
    for i in range(config.args.number):
        perso = Perso(
            first_name=config.args.first_name,
            last_name=config.args.last_name
            )
        print(perso.ascii_art())