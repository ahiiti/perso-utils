#!/usr/bin/env python3

from setuptools import setup

setup(
    name = 'perso_utils',
    version = '0.0',
    description = 'Utilities for the german ID card ("Personalausweis")',
    platforms = ['Linux'],
    author = 'ahiiti',
    url='https://gitlab.com/ahiiti/perso-utils',
    license='GPL3',
    packages=['perso_utils'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'perso-utils = perso_utils.main:main'
        ]
    }
)