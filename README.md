perso-utils
===========

Utilities for the german ID card ("Personalausweis").

Installation
------------

    ./setup.py install --user

Command line usage
------------------

Generate random ID cards:

    perso-utils print

Use it in your python project
-----------------------------

Import the class `Perso`:

    from perso_utils.perso import Perso

Generate a `Perso` object with random values:

    perso = Perso()

Generate a `Perso` object with some manual parameters:

    perso = Perso(first_name='Arnold', last_name='Schwarzenegger')

Use `help(Perso)` for a list of parameters.

Get the ID number of a `Perso` object:

    perso.id_number()

Get the machine-readable area of the ID card:

    perso.mr_area()

Get an ASCII-Art representation of the ID card:

    perso.ascii_art()

See `help(Perso)` for a list of properties.